def under(p):
    # x = p % 3 == 0
    # o = p % 5 == 0
    if p % 3 == 0 and p % 5 == 0:
        return "FizzBuzz"
    elif p % 3 == 0:
        return "Fizz"
    elif p % 5 == 0:
        return "Buzz"
    
        
p = int(input())
print(under(p))